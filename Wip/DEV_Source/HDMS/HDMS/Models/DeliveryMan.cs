//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HDMS.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class DeliveryMan
    {
        public DeliveryMan()
        {
            this.DeliveryMenInPlans = new HashSet<DeliveryMenInPlan>();
        }
    
        public int DeliveryMenId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Nullable<int> Status { get; set; }
    
        public virtual ICollection<DeliveryMenInPlan> DeliveryMenInPlans { get; set; }
    }
}
