//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HDMS.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Cargo
    {
        public int CargoId { get; set; }
        public string Address { get; set; }
        public Nullable<int> CargoColumn { get; set; }
        public int PlanId { get; set; }
        public Nullable<int> CargoType { get; set; }
        public Nullable<int> RequestId { get; set; }
        public Nullable<int> OrderId { get; set; }
    
        public virtual Order Order { get; set; }
        public virtual Plan Plan { get; set; }
        public virtual Request Request { get; set; }
    }
}
