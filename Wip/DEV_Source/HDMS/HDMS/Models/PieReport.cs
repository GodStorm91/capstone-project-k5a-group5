﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HDMS.Models
{
    public class PieReport
    {
        public string Labels { get; set; }
        public string Numbers { get; set; }
        public string Cityname { get; set; }
    }
}