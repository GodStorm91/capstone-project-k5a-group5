﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SMDH.Models.Statuses
{
    public enum CargoTypes
    {
        CollectionPlan = 0,
        DeliveryPlan = 1,
        ReturnedPlan = 2
    }

    public enum PlanTypes
    {
        CollectionPlan = 0,
        DeliveryPlan = 1,
        ReturnedPlan = 2
    }
}